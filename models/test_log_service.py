from flask import Flask
from .log_service import LogService
import os
import pytest
import shutil

app = Flask(__name__)

app.config.from_pyfile(os.path.join("..", "config/app.conf"), silent=False)

root_path_log_files = app.config.get("TEST_LOG_PATH")

data_sample_dir_path = os.path.join(os.getcwd(), "samples")

# Create log management service
log_service = LogService(base_path=root_path_log_files)


@pytest.fixture()
def log_sample_file():
    full_path = os.path.join(data_sample_dir_path, "bigone.log")
    log_file = os.open(full_path)
    return log_file


@pytest.mark.unit
def test_id_to_path():
    # Should we test what happens if id is None or emptied
    # Should we also test what happens if id is not of string type
    fake_id_mol = 'thisIsAFakeIdMol'
    expected_path = 't/h/i/s/I/s/A/F/a/k/e/I/d/M/o/l/'

    log_path = LogService.id_to_path(fake_id_mol)

    assert expected_path == log_path


def test_add_log():
    # add_log_file_from_param(self, id_mol, log_file_name, log_file_data)
    # add_log_file_from_json(self, id_mol, log_file_src)

    pass


def disabled_test_empty_path():
    # define folders to create
    new_path = LogService.id_to_path("fake")

    # define the whole path to create folders in
    full_new_path = os.path.join(
        log_service.base_path,
        new_path
    )

    # create the folders then delete it
    if not os.path.exists(full_new_path):
        os.makedirs(full_new_path)

    was_created = os.path.exists(full_new_path)
    log_service.delete_empty_path(new_path)

    # tmp_log_service = LogService(base_path=tmp_path)
    # tmp_log_service.delete_empty_path(new_path)

    assert was_created
    assert not os.path.exists(new_path)


def disabled_test_delete_log(log_sample_file):
    # Define the log file name and data from the POST request.
    log_file_data = ["mol_log"].read().decode("utf-8")
    log_file_name = log_sample_file["mol_log"].filename

    # delete log file
    fake_id = "fake"
    fake_log_path = LogService.id_to_path(fake_id)

    os.makedirs(os.path.join(log_service.base_path), fake_log_path)

    os.write(
        os.join(log_service.base_path, fake_log_path, log_file_name),
        log_file_data
    )

    assert True
    # log_service.delete_log_file(fake_id, log_file_name)

    # Check that log_file exists and is a file
    # assert not os.path.isfile(os.path.join(
    #    log_service.base_path,
    #    fake_log_path,
    #    log_file_name
    #))
