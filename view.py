import os
import json

from flask import Flask, jsonify, request

from elasticsearch import Elasticsearch
import elasticsearch.exceptions

from models.molecule_service import MoleculeService
from models.log_service import LogService

app = Flask(__name__)

app.config.from_pyfile(os.path.join(".", "config/app.conf"), silent=False)

# Connexion au client Elasticsearch
elasticClient = Elasticsearch(app.config.get("ES_URL"))

# Define root path for log files
root_path_log_files = app.config.get("APP_LOG_PATH")

# Pass client to elasticsearch model classes
molecule_service = MoleculeService(client=elasticClient)
log_service = LogService(base_path=root_path_log_files)


@app.errorhandler(404)
def resource_not_found(e):
    """ Error 404 handler. """
    return jsonify({'error': 'Wrong url, resource not found!'}), 404


@app.route('/api/search/<formula>', methods=['GET'])
def search_molecule(formula):
    """
    Route to search a molecule with its formula in Elasticsearch. 
    """

    # TODO: Should handle possible elasticsearch related and any other exceptions
    results = molecule_service.search(formula)

    # TODO: Should check hits.hits exists

    # Return the results formatted in Json, e.g. status code = 200.
    return jsonify(results['hits']['hits']), 200


@app.route('/api/details/<id_mol>', methods=['GET'])
def details_molecule(id_mol):
    """
    Route to retrieve a molecule with its ID in Elasticsearch.
    """
    try:
        results = molecule_service.get_details(id_mol=id_mol)
    except elasticsearch.exceptions.NotFoundError:
        # Return the error message, e.g. status code = 404.
        return jsonify({'error': 'Molecule with id = \'' +
                        id_mol + '\' does not exists!'}), 404

    # Return the results formatted in Json, e.g. status code = 200.
    return jsonify(results), 200


@app.route('/api/add', methods=['POST'])
def add_molecule():
    """ Route to add a new molecule in Elasticsearch. """
    # Define the files given in the POST request.
    files = request.files

    # Check if the Json is given in the POST request.
    if 'mol_json' not in files:
        # Return the error message, e.g. status code = 400.
        return jsonify(
            {'error': 'You must provide a body for the molecule!'}), 400

    body = files["mol_json"].read().decode("utf-8")

    # Check if the log file is absent from the POST request.
    if 'mol_log' not in files:
        body_json = json.loads(body)

        # TODO: Should check that json is of dict type

        # TODO: Should check that json contains metadata.log_file

        # Reverse split on the right Json line in order
        # to get the source path and the file name.
        log_file_path = body_json['metadata']['log_file'].rsplit("/", 1)

        # Generate the modified Json with the new path.
        body_json['metadata']['log_file'] = log_file_path[1]
        body = json.dumps(body_json)

    # Try the INDEX request on the Elasticsearch client.
    # index and doc_type are specific at the molecules documents.
    try:
        results = molecule_service.add(molecule=body)
    except Exception as err:
        return jsonify(
            {'error': 'Issue while creating molecule Index ' + str(err)}), 400

    # Check if the log file is absent from the POST request.
    try:
        if 'mol_log' in files:
            # Define the log file name and data from the POST request.
            log_file_data = files["mol_log"].read().decode("utf-8")
            log_file_name = files["mol_log"].filename

            # Call the function for the log file creation with log file.
            log_service.add_log_file_from_param(
                results["_id"],
                log_file_name,
                log_file_data
            )
        else:
            # Call the function for the log file creation with log file source
            # path.
            log_service.add_log_file_from_json(results["_id"], log_file_path)
    except Exception as err:
        # If an error is raised, we delete the molecule and print the error
        # message, e.g. status code 500.
        molecule_service.delete(id_mol=results["_id"])

        return jsonify(
            {'error': 'An error occured during the log file creation! ## ' + str(err)}), 500

    # Return the results formatted in Json, e.g. status code = 201.
    return jsonify(results), 201


@app.route('/api/delete/<id_mol>', methods=['DELETE'])
def delete_molecule(id_mol):
    """ Route to delete a molecule with its ID in Elasticsearch. """
    try:
        # Retrieve the molecule document
        molecule_detail = molecule_service.get_details(id_mol=id_mol)

        # Delete the molecule
        results = molecule_service.delete(id_mol=id_mol)

        # Extract the log file name from the molecule document
        log_file_name = molecule_detail['_source']['metadata']['log_file']

        # Delete the log file
        log_service.delete_log_file(id_mol, log_file_name)

        # Return the results formatted in Json, e.g. status code = 200.
        return jsonify(results), 200
    except elasticsearch.exceptions.NotFoundError:
        # Return the error message, e.g. status code = 404.
        return jsonify(
            {
                'error': 'Molecule with id = \'' + id_mol + '\' does not exists!'
            }
        ), 404

    except KeyError:
        return jsonify(
            {
                'error': 'metadata json key which should contain log filename was not found'
            }
        ), 500


if __name__ == "__main__":
    app.run(host='0.0.0.0')
